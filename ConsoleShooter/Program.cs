﻿using System;
using System.Collections.Generic;

namespace ConsoleShooter
{
    class Program
    {
        static void Main(string[] args)
        {
            Players player1 = new Player1();
            Players player2 = new Player2();

            player1.ShowInfo("player1", 100, 5, 100);
            player1.SelectWeapons();
            player1.SelectRangeWeapons();
            Console.WriteLine("==============");
            player2.ShowInfo("player2", 80, 2, 80);
            player2.SelectWeapons();
            player2.SelectRangeWeapons();
            Console.WriteLine("==============");
        }

        class Weapons
        {
            protected int Damage;
            protected int Range;
            public void SetDamage(int Damage)
            {
                this.Damage = Damage;
            }
            public void SetRange(int Range)
            {
                this.Range = Range;
            }

            public virtual void ShowWeapon()
            {
                Console.WriteLine("------------");       
            }
        }
        class Knife : Weapons
        {
            private string knife;
            public Knife()
            {
                knife = "Knife";
                Damage = 10;
                Range = 1;
            }

            public override void ShowWeapon()
            {
                base.ShowWeapon();
                Console.WriteLine(knife);
                Console.WriteLine("Damage:" + Damage);
                Console.WriteLine("Range" + Range);
            }
        }
        class Axe : Weapons
        {
            private string axe;
            public Axe()
            {
                axe = "Axe";
                Damage = 50;
                Range = 1;
            }

            public override void ShowWeapon()
            {
                base.ShowWeapon();
                Console.WriteLine(axe);
                Console.WriteLine("Damage:" + Damage);
                Console.WriteLine("Range" + Range);
            }
        }
        class RangeWeapons
        {
            protected int Damage;
            protected int ShotSpeed;
            protected int Range;
            protected int Patrons;

            public void SetDamage(int Damage)
            {
                this.Damage = Damage;
            }
            public void SetShotSpeed(int ShotSpeed)
            {
                this.ShotSpeed = ShotSpeed;
            }

            public void SetRange(int Range)
            {
                this.Range = Range;
            }

            public void SetPatrons(int Patrons)
            {
                this.Patrons = Patrons;
            }

            public virtual void ShowRangeWeapon()
            {
                Console.WriteLine("-----------");
            }
            
        }
        class Pistol : RangeWeapons
        {
            private string pistol;

            public Pistol()
            {
                pistol = "Pistol";
                Damage = 20;
                ShotSpeed = 25;
                Range = 800;
                Patrons = 12;
            }

            public override void ShowRangeWeapon()
            {
                base.ShowRangeWeapon();
                Console.WriteLine(pistol);
                Console.WriteLine("Damage:"+ Damage);
                Console.WriteLine("ShotSpeed:"+ ShotSpeed);
                Console.WriteLine("Range:"+ Range);
                Console.WriteLine("Patrons:" + Patrons);
            }
        }
        class Machine : RangeWeapons
        {
            private string machine;

            public Machine()
            {
                machine = "Machine";
                Damage = 30;
                ShotSpeed = 50;
                Range = 1500;
                Patrons = 30;
            }

            public override void ShowRangeWeapon()
            {
                base.ShowRangeWeapon();
                Console.WriteLine(machine);
                Console.WriteLine("Damage:" + Damage);
                Console.WriteLine("ShotSpeed:" + ShotSpeed);
                Console.WriteLine("Range:" + Range);
                Console.WriteLine("Patrons:" + Patrons);
            }
        }

        class Players
        {
            protected Weapons weapons = new Knife();
            protected Weapons weapons1 = new Axe();
            protected RangeWeapons rangeweapons = new Pistol();
            protected RangeWeapons rangeweapons1 = new Machine();

            protected string Name;
            protected int Health;
            protected int Kill;
            protected int SpeedMove;

            public virtual void ShowInfo(string Name, int Health,int Kill,int SpeedMove)
            {
                this.Name = Name;
                this.Health = Health;
                this.Kill = Kill;
                this.SpeedMove = SpeedMove;

                Console.WriteLine("Name:"+ Name);
                Console.WriteLine("Health:" + Health);
                Console.WriteLine("Kill:" + Kill);
                Console.WriteLine("SpeedMove:" + SpeedMove);
            }
            public virtual void SelectWeapons()
            {
                Console.WriteLine("-------------");
                Console.WriteLine("Select Weapons:\n 1. Knife\n 2. Axe");
                int Select = Convert.ToInt32(Console.ReadLine());

                if (Select == 1)
                {
                    weapons.ShowWeapon();

                }

                if (Select == 2)
                {
                    weapons1.ShowWeapon();
                }


            }
            public virtual void SelectRangeWeapons()
            {
                Console.WriteLine("-------------");
                Console.WriteLine("Select RangeWeapons:\n 1. Pistol\n 2. Machine");
                int Select = Convert.ToInt32(Console.ReadLine());

                if (Select == 1)
                {
                    rangeweapons.ShowRangeWeapon();
                }

                if (Select == 2)
                {
                    rangeweapons1.ShowRangeWeapon();
                }
            }



        }
        class Player1 : Players
        {
            public override void ShowInfo(string Name, int Health, int Kill, int SpeedMove)
            {
                Player1 player1 = new Player1();
                base.ShowInfo("player1", 100, 5, 100);
            }
            public override void SelectWeapons()
            {
                base.SelectWeapons();
            }
            public override void SelectRangeWeapons()
            {
                base.SelectRangeWeapons();
            }
        }
        class Player2 : Players
        {

            
            public override void ShowInfo(string Name, int Health, int Kill, int SpeedMove)
            {

                base.ShowInfo("player2", 80, 2, 80);
            }
            public override void SelectWeapons()
            {
                base.SelectWeapons();
            }
            public override void SelectRangeWeapons()
            {
                base.SelectRangeWeapons();
            }
        }

    }
}
